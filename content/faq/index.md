title: Frequently asked questions
---

# FAQ - Common-Lisp.Net Frequently asked questions

{table-of-contents :depth 3 :start 2 :label "Table of contents" }

## GitLab

### Configure e-mail on push

GitLab supports sending an e-mail upon pushing
commits by project members.  In order to use the functionality,
please follow the steps outlined in
[configuration guide](/faq/emailonpush).

### Deploy project pages

Use GitLab Pages to deploy your project pages
to `https://common-lisp.net/project/<project>/`
using our [deployment instructions](/faq/using-gitlab-deploy-project-pages).
